import { extractNumbers } from './src/parser.js';
import {
  validateStringNotEmpty,
  validateNumber
} from './src/util/validation.js';
import { add } from './src/math.js';
import { transformToNumber } from './src/util/numbers.js';

const form = document.querySelector('form');
const output = document.getElementById('result');

function formSubmitHandler(event) {
  event.preventDefault();
  const formData = new FormData(form);
  const numberInputs = extractNumbers(formData);
  const numbers = getNumbers(numberInputs);
  const result = getResult(numbers);
  let resultText = showResult(result);
  output.textContent = resultText;
}

export function getNumbers(numberInputs) {
  const numbers = [];
  for (const numberInput of numberInputs) {
    console.log(numberInput)
    console.log(typeof numberInput)
    validateStringNotEmpty(numberInput);
    const number = transformToNumber(numberInput);
    validateNumber(number);
    numbers.push(number);
  }
  return numbers;
}

export function getResult(numbers){
  let result = '';
  try {
    result = add(numbers).toString();
  } catch (error) {
    result = error.message;
  }
  return result;
}

export function showResult(result) {
  let resultText = '';
  if (result === 'invalid') {
    resultText = 'Invalid input. You must enter valid numbers.';
  } else if (result !== 'no-calc') {
    resultText = 'Result: ' + result;
  }
  return resultText;
}

form.addEventListener('submit', formSubmitHandler);
