import { describe, expect, it } from 'vitest';
import { getNumbers } from './app';
import { getResult } from './app';
import { showResult } from './app';

describe('Get numbers from inputs', () => {
  it('correct extraction', () => {
    expect( getNumbers(['5', '3'])).to.eql([5, 3]);
  })
})

describe('Get result', () => {
  it('correct extraction', () => {
    expect( getResult([5, 3])).to.eql('8');
  })
})

describe('Show result', () => {
  it('correct result', () => {
    expect( showResult(13)).to.eql('Result: 13');
  })
  it('incorrect result', () => {
    expect( showResult('invalid')).to.eql('Invalid input. You must enter valid numbers.');
  })
})