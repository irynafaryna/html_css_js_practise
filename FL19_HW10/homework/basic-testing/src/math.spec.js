import { describe, expect, it } from 'vitest'
import { add } from './math';

describe('The sum of two numbers', () => {
  it('correct addition', () => {
    expect(add([5, 3])).to.eql(8);
  })
})