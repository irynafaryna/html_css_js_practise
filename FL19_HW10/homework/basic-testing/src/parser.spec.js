import { describe, expect, it } from 'vitest';
import { extractNumbers } from './parser';
//import FormData from 'form-data'; 

describe('Extract numbers', () => {
  // const formdata = new FormData(); 
  // formdata.append('num1','5'); 
  // formdata.append('num3','3'); 
  // it('correct extraction', () => { 
  //   expect( extractNumbers(formdata)).to.eql(['5', '3']); 
  // }) 
  let formData = {
    num1: '5',
    num2 : '3',
    get: function(key){
      return this[key];
    }
  }
  it('correct extraction', () => {
    expect( extractNumbers(formData)).to.eql(['5', '3']);
  })
})