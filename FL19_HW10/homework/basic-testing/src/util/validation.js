export function validateStringNotEmpty(value) {
  if (value.trim().length !== 0) {
  return true; 
  } else {
    throw new Error('Invalid input - must not be empty.');
  }
}

export function validateNumber(number) {
  if (isNaN(number)) {
    throw new Error('Invalid number input.');
  }
}