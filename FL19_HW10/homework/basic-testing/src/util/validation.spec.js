import { describe, expect, it } from 'vitest';
import { validateStringNotEmpty, validateNumber } from './validation';

describe('Validate string', () => {
  it('correct validation', () => {
    expect(validateStringNotEmpty('5')).toBeTruthy();
  })

  it('invalid validation', () => {
    expect(() => validateStringNotEmpty('')).toThrowError('Invalid input - must not be empty.');
  })
})

describe('Validate number', () => {
  it('invalid validation', () => {
    expect(() => validateNumber('g')).toThrowError('Invalid number input.');
  })
})
