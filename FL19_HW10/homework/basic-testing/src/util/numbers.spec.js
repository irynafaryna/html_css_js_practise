import { describe, expect, it } from 'vitest';
import { transformToNumber } from './numbers';

describe('Tranform to Number', () => {
  it('correct transormation', () => {
    expect(transformToNumber('5')).to.eql(5);
  })
})
