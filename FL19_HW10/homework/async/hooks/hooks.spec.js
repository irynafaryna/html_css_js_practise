import { describe, test, expect } from 'vitest';
import { User } from './hooks';

describe( ' User: update email', () => {
  const user = new User();
  test('function should be defined', () => {
    expect(user.updateEmail).toBeDefined() 
  })

  test('correct updating'), () => {
    user.updateEmail('iryna@gmail.com');
    console.log('ffs', user.email)
    expect(user.email).to.eql('iryna@gmail.com') 
  }
})
