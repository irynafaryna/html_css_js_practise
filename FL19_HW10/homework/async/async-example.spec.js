import {generateToken} from './async-example';
import {describe, test, vi, expect} from 'vitest';

describe( 'Generate token', () => {
  test('correct generation', () => new Promise(done => {
    const spy = vi.spyOn(console, 'log');
    function callback(error, data) {
      if (error) {
      done(error);
      return;
      }
      try {
      expect(spy).toHaveBeenCalledWith(data);
      done();
      } catch (error) {
      done(error);
      }
      }
      generateToken('123', callback);
  }));
})



