const requestURL = 'https://jsonplaceholder.typicode.com/users'

const btnJS = document.getElementById('btn-js');
const btnFetch = document.getElementById('btn-fetch');
const fetchBlock = document.getElementById('fetch-block');

btnJS.onclick = loadByJS;
btnFetch.onclick = loadByFetch;

function loadByJS(){
  return new Promise( (resolve, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.open('GET', requestURL);

    xhr.responseType = 'json';

    xhr.onload = () => {
      if(xhr.status >= 400){
        reject(xhr.response)
      } else {
        resolve(xhr.response)
      }
    }

    xhr.onerror = () => {
      reject(xhr.response);
    }

    xhr.send();
  })
  .then(data => {
    let n = 0;
    data.forEach(element => {
      const jsBlock = document.getElementById('js-block');
      const newElement = document.createElement('p')
      n += 1;
      newElement.setAttribute('id', n)
      jsBlock.appendChild(newElement);
      newElement.innerHTML = element.name;
    })   
  });
}

function loadByFetch(){
  return fetch(requestURL).then(response => {
      return response.json();
  })
    .then(fetchdata => {
      let elementId = 20;
      let editbtnId = 40;
      let deletebtnId = 50;
      let sectionId = 60;
      fetchdata.forEach(element => {
        const section = document.createElement('div');
        sectionId += 1;
        section.setAttribute('id', sectionId);
        fetchBlock.appendChild(section);

        const newElement = document.createElement('p');
        elementId += 1;
        newElement.setAttribute('id', elementId);
        newElement.innerHTML = element.name;
        section.appendChild(newElement);
        
        const btnblock = document.createElement('div');
        btnblock.setAttribute('class', 'btnblock');
        section.appendChild(btnblock);

        const editbtn = document.createElement('button');
        editbtnId += 1; 
        editbtn.setAttribute('id', editbtnId);
        editbtn.setAttribute('class', 'editbtn');
        editbtn.innerText = 'Edit';
        btnblock.appendChild(editbtn);
        
        const deletebtn = document.createElement('button');
        deletebtnId += 1;
        deletebtn.setAttribute('id', deletebtnId);
        deletebtn.setAttribute('class', 'deletebtn');
        deletebtn.innerText = 'Delete';
        btnblock.appendChild(deletebtn);
      })  
    })
}

let editInputId = 90;
fetchBlock.onclick = function(event){
  const editblock = document.createElement('div');
  let parentId = event.target.parentNode.parentNode.id;
  if (event.target.className === 'editbtn'){
    if(!event.target.parentNode.nextSibling){
      const editsection = document.getElementById(parentId);
      const editInput = document.createElement('input');
      const savebtn = document.createElement('button');
      editInputId += 1;
      editInput.setAttribute('class', 'editInput');
      editInput.setAttribute('id', editInputId);

      editInput.value = event.target.parentNode.parentNode.firstChild.textContent;
      let oldValue = event.target.parentNode.parentNode.firstChild;
      
      savebtn.onclick = function(event){
        let newValue = event.target.previousSibling.value;
        oldValue.textContent = newValue;
      }

      savebtn.setAttribute('class', 'savebtn');
      savebtn.innerText = 'Save';

      editblock.setAttribute('class', 'editblock');
      editblock.appendChild(editInput);
      editblock.appendChild(savebtn);
      editsection.appendChild(editblock);
    }  
  }    
}

