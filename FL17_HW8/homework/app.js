const appRoot = document.getElementById('app-root');
let countryList = [];
let sortName = 'asc';
let sortArea = 'asc';

let header = document.createElement('header');
  h1 = document.createElement('h1');
  h1.textContent = 'Countries Search';
  header.appendChild(h1);

appRoot.appendChild(header);

let buttonsBlock = document.createElement('div');
  buttonsBlock.setAttribute('class', 'buttons-block');
  p1 = document.createElement('p');
  p1.textContent = 'Please choose the type of search: '
  buttonsBlock.appendChild(p1);

let buttons = document.createElement('div');
buttons.setAttribute('class', 'buttons')

let regionBlock = document.createElement('div');

let btnRegion = document.createElement('INPUT');
  btnRegion.setAttribute('type', 'radio');
  btnRegion.setAttribute('name', 'radio');
  btnRegion.setAttribute('class', 'byreg');

let btnRegionName = document.createElement('Label');
  btnRegionName.textContent = 'By Region';
 
regionBlock.appendChild(btnRegion);
regionBlock.appendChild(btnRegionName);
buttons.appendChild(regionBlock);

let languageBlock = document.createElement('div');

let btnLanguage = document.createElement('INPUT');
  btnLanguage.setAttribute('type', 'radio');
  btnLanguage.setAttribute('name', 'radio');
  btnLanguage.setAttribute('class', 'bylag');
  
let btnLanguageName = document.createElement('Label');
  btnLanguageName.textContent = 'By Language';
  
languageBlock.appendChild(btnLanguage);
languageBlock.appendChild(btnLanguageName);
buttons.appendChild(languageBlock);

buttonsBlock.appendChild(buttons);

appRoot.appendChild(buttonsBlock);

let selectBlock = document.createElement('div');
  p2 = document.createElement('p');
  p2.textContent = 'Please choose search query: '
  selectBlock.appendChild(p2);

let select = document.createElement('SELECT');
  select.setAttribute('id', 'mySelect');
  selectBlock.appendChild(select);
  addDefaultSelectValue()

appRoot.appendChild(selectBlock);

let tableBlock = document.createElement('div');
  tableBlock.setAttribute('id', 'tableBlock');

  let table = document.createElement('table');
  table.setAttribute('id', 'countries');
  table.setAttribute('class', 'table table-sortable');
  tableBlock.appendChild(table);

appRoot.appendChild(tableBlock);


// listener for select
btnRegion.addEventListener('click', function () {
  removeOptions(select)
  addDefaultSelectValue()
  blockContent()
  
  let regions = externalService.getRegionsList();
  return regions.forEach((element) => {
    let newvalue = document.createElement('option');
    newvalue.setAttribute('value', element);
    let sv1 = document.createTextNode(element)
    newvalue.appendChild(sv1)
    return select.appendChild(newvalue)
  });
});

btnLanguage.addEventListener('click', function () {
  removeOptions(select)
  addDefaultSelectValue()
  blockContent()

  let language = externalService.getLanguagesList();
  return language.forEach((element) => {
    let newvalue = document.createElement('option');
    newvalue.setAttribute('value', element);
    let sv1 = document.createTextNode(element)
    newvalue.appendChild(sv1)
    return select.appendChild(newvalue)
  });
});

// listener for table
select.addEventListener('change', function (el) {
  blockContent()

  if (btnRegion.checked) {
    countryList = externalService.getCountryListByRegion(el.target.value) 
  } else {
    countryList = externalService.getCountryListByLanguage(el.target.value)
  }

  if (countryList.length) {
    addTable(countryList);
  }
  
})

//sort

function sortCountryListByName(sortDirection) {
  let sortedName = [];

  if (sortDirection === 'asc') {
    sortedName = countryList.sort(function(a, b){
      let nameA = a.name.toUpperCase(); 
      let nameB = b.name.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
  } else if (sortDirection === 'desc') {
    sortedName = countryList.sort(function(a, b){
      let nameA = a.name.toUpperCase(); 
      let nameB = b.name.toUpperCase();
      if (nameA < nameB) {
        return 1;
      }
      if (nameA > nameB) {
        return -1;
      }
      return 0;
    });
  }

  sortName = sortName === 'asc' ? 'desc' : 'asc';

  addTable(sortedName)
}

function sortCountryListByArea(sortDirection) {
  let sortedArea = [];

  if (sortDirection === 'asc') {
    sortedArea = countryList.sort(function (a, b) {
      return a.area - b.area;
    })
  } else if (sortDirection === 'desc') {
      sortedArea = countryList.sort(function (a, b) {
        return b.area - a.area;
      })
  }

  sortArea = sortArea === 'asc' ? 'desc' : 'asc';
  
  addTable(sortedArea)
}

// helpers
function removeOptions(selectElement) {
  let i, L = selectElement.options.length - 1;
  for(i = L; i >= 0; i--) {
     selectElement.remove(i);
  }
}

function addDefaultSelectValue() {
  let value = document.createElement('option');
  let defaultValue = document.createTextNode('Select value');
  value.appendChild(defaultValue);
  select.appendChild(value);
}

function addTable(countryList) {
  if (document.getElementById('message')) {
    tableBlock.removeChild(document.getElementById('message'))
  }
 
  if (table.getElementsByTagName('thead')[0]) {
    table.removeChild(table.getElementsByTagName('thead')[0]);
  }
  
  let thead = document.createElement('thead');
  let tr = document.createElement('tr');

  let th1 = document.createElement('th');
  th1.setAttribute('class', 'th-country')
  let countryHeader = document.createElement('div')
  countryHeader.textContent = 'Country name';
  th1.appendChild(countryHeader)
  tr.appendChild(th1);

  let arrow = document.createElement('div')
  arrow.setAttribute('class', 'arrow')
  let arrowUp = document.createElement('div')
  arrowUp.setAttribute('class', 'arrow-up')
  arrowUp.textContent = '^'
  let arrowDown = document.createElement('div')
  arrowDown.setAttribute('class', 'arrow-down')
  arrowDown.textContent = '^'
  th1.appendChild(arrow)
  arrow.appendChild(arrowUp)
  arrow.appendChild(arrowDown)

  arrow.addEventListener('click', function(){
    sortCountryListByName(sortName === 'asc' ? 'desc' : 'asc');
  })
  
  let th2 = document.createElement('th');
  th2.textContent = 'Capital';
  tr.appendChild(th2);
  
  let th3 = document.createElement('th');
  th3.textContent = 'World Region';
  tr.appendChild(th3);
  
  let th4 = document.createElement('th');
  th4.textContent = 'Languages';
  tr.appendChild(th4);
  
  let th5 = document.createElement('th');
  th5.setAttribute('class', 'th-area')
  let areaHeader = document.createElement('div')
  areaHeader.textContent = 'Area';
  th5.appendChild(areaHeader)
  tr.appendChild(th5);

  let arrowArea = document.createElement('div')
  arrowArea.setAttribute('class', 'arrowArea')
  let arrowAreaUp = document.createElement('div')
  arrowAreaUp.setAttribute('class', 'arrowArea-up')
  arrowAreaUp.textContent = '^'
  let arrowAreaDown = document.createElement('div')
  arrowAreaDown.setAttribute('class', 'arrowArea-down')
  arrowAreaDown.textContent = '^'
  th5.appendChild(arrowArea)
  arrowArea.appendChild(arrowAreaUp)
  arrowArea.appendChild(arrowAreaDown)

  arrowArea.addEventListener('click', function(){
    sortCountryListByArea(sortArea === 'asc' ? 'desc' : 'asc')
  })
  
  let th6 = document.createElement('th');
  th6.textContent = 'Flag';
  tr.appendChild(th6);
  
  thead.appendChild(tr);
  table.appendChild(thead);
  tableBlock.appendChild(table);

if (table.getElementsByTagName('tbody')[0]) {
  table.removeChild(table.getElementsByTagName('tbody')[0]);
}
  
let tbody = document.createElement('tbody');
  tbody.setAttribute('id', 'tbody');

  countryList.forEach((country) => {

  let tr = document.createElement('tr');

  let td1 = document.createElement('td');
  td1.textContent = country.name;
  tr.appendChild(td1);
  
  let td2 = document.createElement('td');
  td2.textContent = country.capital;
  tr.appendChild(td2);
  
  let td3 = document.createElement('td');
  td3.textContent = country.region;
  tr.appendChild(td3);
  
  let td4 = document.createElement('td');
  let lenguagesList = [];
  Object.keys(country.languages).forEach((key) => lenguagesList.push(country.languages[key]))
  td4.textContent = lenguagesList.join(', ');
  tr.appendChild(td4);
  
  let td5 = document.createElement('td');
  td5.textContent = country.area;
  tr.appendChild(td5);
  
  let td6 = document.createElement('td');
  let image = document.createElement('img');
  image.setAttribute('src', country.flagURL);
  td6.appendChild(image);
  tr.appendChild(td6);

  tbody.appendChild(tr);
  })
  table.appendChild(tbody);
}

function blockContent(){
  if (!table.getElementsByTagName('tbody')[0] && !document.getElementById('message')) {
    message = document.createElement('p');
    message.setAttribute('id', 'message')
    message.textContent = 'No items, please choose search query'
    return tableBlock.appendChild(message);
  } else if (table.getElementsByTagName('tbody')[0] && document.getElementById('message')) {
    return tableBlock.removeChild(message)
  }
}

