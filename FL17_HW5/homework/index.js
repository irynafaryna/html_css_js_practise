function isEquals(a,b){
  return a===b
}

function isBigger(a,b){
  return a > b;
}

function storeNames(){
  let arr = [...arguments];
  return arr;
}

function getDifference(a, b){
  if (b > a){
    return b-a;
  } else{
    return a-b;
  }
}

function negativeCount(arr){
  let count = 0;
  for ( let i = 0; i <= arr.length; i++){
    if(arr[i] < 0){
      count += 1;
    } 
  }
  return count;
}

function letterCount(str1, str2){
  let arr = str1.split('');
  let count = 0;
  for (let i = 0; i <= arr.length; i++){
    if(str2 === arr[i]){
      count += 1;
    }
  }
  return count;
}

function countPoints(arr){
  let count = 0;
  arr.forEach(element => {
    let array = element.split(':');
    if (+array[0] > +array[1]){
      count +=3;
    } else if (+array[0] === +array[1]){
      count +=1;
    } else {
      count +=0;
    }
  });
  return count;
}


