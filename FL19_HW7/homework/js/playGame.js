import {winner} from './winner.js';
import {gameOver} from './gameOver.js';
import {moves} from './main.js';

export const playGame = () => {
	const rockBtn = document.querySelector('.rock');
	const paperBtn = document.querySelector('.paper');
	const scissorBtn = document.querySelector('.scissor');
	const playerOptions = [rockBtn, paperBtn, scissorBtn];
	const computerOptions = ['rock', 'paper', 'scissors']
	
	playerOptions.forEach(option => {
		option.addEventListener('click',function(){

			moves++;
			const movesLeft = 3-moves;
			const choiceNumber = Math.floor(Math.random()*3);
			const computerChoice = computerOptions[choiceNumber];

			winner(this.innerText, computerChoice, moves)
			if(moves == 3){
				gameOver(playerOptions, movesLeft);
			}
		})
	})
}