import {resetBtn} from './main.js';
import {playerScore} from './main';
import {computerScore} from './main';

export const gameOver = (playerOptions, movesLeft) => {
	const chooseMove = document.querySelector('.move');
	const result = document.querySelector('.result');
	const reloadBtn = document.querySelector('.reload');
	
	resetBtn.style.display = 'none';

	playerOptions.forEach(option => {
		option.style.display = 'none';
	})
	
	chooseMove.innerText = 'Game Over!!'

	if(playerScore > computerScore){
		result.style.fontSize = '2rem';
		result.innerText = 'You Won The Game'
		result.style.color = '#308D46';
	}
	else if(playerScore < computerScore){
		result.style.fontSize = '2rem';
		result.innerText = 'You Lost The Game';
		result.style.color = 'red';
	}
	else{
		result.style.fontSize = '2rem';
		result.innerText = 'Tie';
		result.style.color = 'grey'
	}
	reloadBtn.innerText = 'Restart';
	reloadBtn.style.display = 'flex'
	reloadBtn.addEventListener('click',() => {
		window.location.reload();
	})
}