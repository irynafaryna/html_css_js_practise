import '../scss/style.scss';
import {playGame} from './playGame.js';

export let playerScore = 0;
export let computerScore = 0;
export let moves = 0;
export const resetBtn = document.querySelector('.reset');
resetBtn.addEventListener('click',() => {
	window.location.reload();
})

playGame();