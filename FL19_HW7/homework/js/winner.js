import {playerScore} from './main';
import {computerScore} from './main';
import {moves} from './main.js';

export const winner = (player,computer) => {
	const result = document.querySelector('.result');
	const playerScoreBoard = document.querySelector('.p-count');
	const computerScoreBoard = document.querySelector('.c-count');
	player = player.toLowerCase();
	computer = computer.toLowerCase();
	if(player === computer){
		result.textContent = 'Round '+ moves + ', ' + player + ' vs. ' + computer + ', TIE!'
	}
	else if(player == 'rock'){
		if(computer == 'paper'){
			result.textContent = 'Round '+ moves + ', ' + player + ' vs. ' + computer + ', You`ve LOST!';
			computerScore++;
		}else{
			result.textContent = 'Round '+ moves + ', ' + player + ' vs. ' + computer + ', You`ve WON!';
			playerScore++;
		}
	}
	else if(player == 'scissors'){
		if(computer == 'rock'){
			result.textContent = 'Round '+ moves + ', ' + player + ' vs. ' + computer + ', You`ve LOST!';
			computerScore++;
		}else{
			result.textContent = 'Round '+ moves + ', ' + player + ' vs. ' + computer + ', You`ve WON!';
			playerScore++;
		}
	}
	else if(player == 'paper'){
		if(computer == 'scissors'){
			result.textContent = 'Round '+ moves + ', ' + player + ' vs. ' + computer + ', You`ve LOST!';
			computerScore++;
		}else{
			result.textContent = 'Round '+ moves + ', ' + player + ' vs. ' + computer + ', You`ve WON!';
			playerScore++;
		}
	}
}
