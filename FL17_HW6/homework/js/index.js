let one = localStorage.getItem('Page1'); 
let two = localStorage.getItem('Page2'); 
let three =localStorage.getItem('Page3');

function visitLink(path) {
	let pageCount = Number(localStorage.getItem(path));
	if (pageCount) {
		pageCount += 1;
	} else {
		pageCount = 1;
	}
	localStorage.setItem(path, pageCount);
}

function viewResults() {
	const content = document.getElementById('content'); 
	const result = document.createElement('content'); 
		
	result.innerHTML = ''; 
	result.insertAdjacentHTML('beforeend',  
	`<ul> 
	<li>You visited Page1 : ${one?one:0} time(s)</li> 
	<li>You visited Page2 : ${two?two:0} time(s)</li> 
	<li>You visited Page3 : ${three?three:0} time(s)</li> 
	</ul>`); 
	content.appendChild(result);
	localStorage.clear();
}

