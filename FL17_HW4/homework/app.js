function reverseNumber(num) {
  let digit, result = 0;

  while( num ){
      digit = num % 10; 
      result = result * 10 + digit;  
      num = num/10|0;  
  }  
  return result;
}

function forEach(arr, func) {
  for (let i = 0; i < arr.length; i++){
    func(arr[i]);
  }
}

function map(arr, func) {
  let newArr = [];
  forEach(arr, function(el){
    newArr.push(func(el));
  });
  return newArr;
}

function filter(arr, func) {
  let newArr = [];
  forEach(arr, function(el){
    if(func(el)){
    newArr.push(el);
    }
  });

  return newArr;
}

function getAdultAppleLovers(data) {
  const newArr = filter(data, function (el) {
    return el.age > 18 && el.favoriteFruit === 'apple'
})
  const arr2 = map(newArr, function(el){
    return el.name;
  });
  return arr2;
}

function getKeys(obj) {
  newArr = [];
  for (let key in obj) {
    newArr.push(key);
  }
  return newArr;
}

function getValues(obj) {
  let keys = [];
  for (let item in obj){
    keys.push(obj[item]);
  }
  return keys;
}

function showFormattedDate(dateObj) {
  let month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  return 'It is ' + dateObj.getDate() + ' of ' + month[dateObj.getMonth()] + ', ' + dateObj.getFullYear();
}

