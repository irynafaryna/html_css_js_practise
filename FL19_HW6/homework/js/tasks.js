function getMaxEvenElement(arr){
  return Math.max(...arr.filter(n => n % 2 === 0));
}

let a = 3;
let b = 5;
[a, b] = [b, a];

function getValue(value){
  return value ?? '-'; 
}

function getObjFromArray(array) {
  return Object.fromEntries(array);
}

function addUniqueId(object){
  return {...object, id: Symbol() };
}

function getRegroupedObject(obj) {
  const { details: { university, age, id }, name: firstName } = obj;
  return {university, user: { age, firstName, id}};
}

function getArrayWithUniqueElements(array){
  let result = [...new Set(array)];
  return result;
}

function hideNumber(number){
  return number.slice(-4).padStart(number.length, '*');
}

function add(a, b = null){
  if(b !== 0 && !b){
    throw new Error('b is required');
  } 
  return a + b;
}

function * generateIterableSequence(){
  yield 'I';
  yield 'love'
  yield 'EPAM'
}
