function totalprize() {
  let totalPrize=0;
  function casino(newUserNumber, newPrize) {
    let result = confirm('Do you want to play a game?');
    if(result) {
      const defaultUserNumber = 9;
      const defaultPrize = {
        1: 100,
        2: 50,
        3: 25
      };

      const userNumber = newUserNumber ? newUserNumber : defaultUserNumber;
      const prize = newPrize ? newPrize : defaultPrize;

      let randomNumber = Math.floor(Math.random() * userNumber);
      let maxTries = 3;
      let counter = 0;

      while (attempts !== randomNumber){
        var attempts = Number(prompt('Choose a roulette pocket number from 0 to ' + (userNumber-1) + randomNumber));
        counter += 1;

        if (counter > maxTries){
            alert('You have no more tries left');
            break
        }

        if (attempts === randomNumber){
            alert('Thank you for your participation. Your prize is: ' + prize[counter] + '$');
            let res = confirm('Do you want to continue?');

            if (res){
              const nextUserNumber = userNumber + 4;
              const nextPrize = {
                1: prize[1]*2,
                2: prize[2]*2,
                3: prize[3]*2
              }
              totalPrize+=prize[counter];
              casino(nextUserNumber, nextPrize);
            } else {
              alert('Thank you for your participation. Your prize is: ' + totalPrize);
            }
        } 
      } 
    } else{
      alert('You did not become a billionaire, but can.');
    }
  }
  casino();
}

