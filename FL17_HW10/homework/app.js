// const tweetListTest = JSON.stringify([
//   { id: '1', text: 'comment', isLiked: true},
//   { id: '2', text: 'comment', isLiked: false }
// ])

// localStorage.setItem('tweetList', tweetListTest)

const tweetListFromLocalStorage = JSON.parse(localStorage.getItem('tweetList'))
let tweetList = tweetListFromLocalStorage ? tweetListFromLocalStorage : [];

const root = document.getElementById('root');

const listBlock = document.getElementById('tweetItems');
const navigationBlock = document.getElementById('navigationButtons');
const addButton = document.querySelector('.addTweet');
const goToLikedListButton = document.createElement('button');
goToLikedListButton.innerText = 'Go to liked';
goToLikedListButton.style.display = 'none';
const list = document.getElementById('list');

const createEditBlock = document.getElementById('modifyItem');
let input = document.getElementById('modifyItemInput');
const saveButton = document.getElementById('saveModifiedItem');
const cancelButton = document.getElementById('cancelModification');
listBlock.appendChild(goToLikedListButton);

let editId;

const isLikedInList = tweetList.some(({ isLiked }) => isLiked);

const islikedHash = window.location.hash === '#liked';

// if (isLikedInList) {
//   goToLikedListButton.style.display = 'inline-block';
// } else {
//   goToLikedListButton.style.display = 'none';
// }

addButton.onclick = function () {
  window.location.hash = '#add';
  listBlock.style.display = 'none';
  createEditBlock.style.display = 'block';
}

// editButton.onclick = function(event) {
//   window.location.hash = `#/edit/:${event.target.id}`;
//   input = e.target.textContent;
//   listBlock.style.display = 'none';
//   createEditBlock.style.display = 'block';
// }


window.onload = function name() {
  history.replaceState(null, null, ' ');
  tweetList.forEach(element => {
    let listElements = document.createElement('li');
      let editButton = document.createElement('span');
      listElements.id = element.id;
      editButton.className = 'editButton';
      editButton.id = element.id;
      editButton.innerText = element.text;
      let removeButton = document.createElement('button');
      removeButton.innerText = 'remove';
      let likeButon = document.createElement('button');
      likeButon.innerText = 'like';
      listElements.append(editButton);
      listElements.append(removeButton);
      listElements.append(likeButon);
      list.append(listElements);
  });
}

saveButton.onclick = function () {
  const value = input.value;
  const isAdd = window.location.hash === '#add';

  if (value.length === 0 || value.length > 140) {
    alertMessage('Error! You can\'t tweet about that');
  }

  if (tweetList.some(({ text }) => text === value)) {
    alertMessage('Error! You can\'t tweet about that');
  }
  const item = {
    id: Math.floor(Math.random() * 100),
    text: value,
    isLiked: false
  }
  if (isAdd) {
    tweetList.push(item)
    localStorage.setItem('tweetList', JSON.stringify(tweetList));
    if (value.length > 0 && value.length < 140) {
      let listElements = document.createElement('li');
      let editButton = document.createElement('span');
      listElements.id = item.id;
      editButton.className = 'editButton';
      editButton.id = item.id;
      editButton.innerText = value;
      let removeButton = document.createElement('button');
      removeButton.innerText = 'remove';
      let likeButon = document.createElement('button');
      likeButon.innerText = 'like';
      listElements.append(editButton);
      listElements.append(removeButton);
      listElements.append(likeButon);
      list.append(listElements);
    }
  } else {
    let urlString = window.location.hash;
    let id = urlString.replace('#/edit/:', '')
    tweetList.map((el) => {
      if (el.id === +id) {
        el.text = input.value;
      }
      return el
    })
    const el = document.getElementById(id)
    el.children[0].innerText = input.value
    localStorage.setItem('tweetList', JSON.stringify(tweetList));
  }

  window.location.hash = '';
  listBlock.style.display = 'block';
  createEditBlock.style.display = 'none';
}

cancelButton.onclick = function () {
  window.location.hash = '';
  listBlock.style.display = 'block';
  createEditBlock.style.display = 'none';
}

function alertMessage(message) {
  const alertMessage = document.getElementById('alertMessage');
  alertMessage.textContent = message;
  alertMessage.classList = 'alertMessage';
  setTimeout(() => {
    alertMessage.classList = 'hidden';
  }, 2000);
}


list.onclick = function (e) {
  if (e.target.innerText === 'remove') {
    tweetList = tweetList.filter((el) => el.id !== +e.target.id);
    list.removeChild(e.target.parentNode);
  }


  if (e.target.innerText === 'like') {

    tweetList.map((el) => {
      if (el.id === +e.target.parentNode.id) {
        el.isLiked = true;
        alertMessage(`Hooray! You liked tweet with id ${el.id}!`);
      }
      return el
    })
    goToLikedListButton.style.display = 'inline-block';
    
  }

  if (e.target.className === 'editButton') {
    window.location.hash = `#/edit/:${e.target.id}`;
    input.value = e.target.textContent;

    listBlock.style.display = 'none';
    createEditBlock.style.display = 'block';

  }
  localStorage.setItem('tweetList', JSON.stringify(tweetList));
}

let LikedList = [];
const backButton = document.createElement('button');
backButton.innerText = 'back';
const likedTweetsDiv = document.createElement('div');

goToLikedListButton.onclick = function () {
  createEditBlock.style.display = 'none';
  listBlock.style.display = 'none';
  likedTweetsDiv.id = 'likedTweetsDiv';
  const likedTweets = document.createElement('h1')
  likedTweets.textContent = 'Liked Tweets'
  likedTweetsDiv.append(likedTweets)
  likedTweetsDiv.append(backButton)
  root.append(likedTweetsDiv)
  window.location.hash = '#liked';
  tweetList.forEach((el) => {
    if (el.isLiked === true) {
      console.log(el.text)
      let ullistLikedElements = document.createElement('ul');
      ullistLikedElements.id = 'ullistLikedElements';
      let listLikedElements = document.createElement('li');
      let editButton = document.createElement('span');
      listLikedElements.id = el.id;
      editButton.className = 'editButton';
      editButton.id = el.id;
      editButton.innerText = el.text;
      let removeButton = document.createElement('button');
      removeButton.innerText = 'remove';
      let likeButon = document.createElement('button');
      likeButon.innerText = 'like';
      listLikedElements.append(editButton);
      listLikedElements.append(removeButton);
      listLikedElements.append(likeButon);
      ullistLikedElements.append(listLikedElements);
      likedTweetsDiv.append(ullistLikedElements);
      
    }
    return el
  })
  
}

backButton.onclick = function() {
  listBlock.style.display = 'block';
  likedTweetsDiv.style.display = 'none';
}