import '../css/style.scss';
import {renderCalendar} from './calendar.js';

export const date = new Date();

renderCalendar();

document.querySelector('.prev').addEventListener('click', () => {
  date.setMonth(date.getMonth() - 1);
  renderCalendar();
});

document.querySelector('.next').addEventListener('click', () => {
  date.setMonth(date.getMonth() + 1);
  renderCalendar();
});