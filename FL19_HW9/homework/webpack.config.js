const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isDev = process.env.NODE_ENV === 'developmant';
const isProd = !isDev;

const filename = (ext) => isDev ? `[name].${ext}` : `[name].[contenthash].${ext}`;
module.exports = {
  mode: 'development',
  entry: './js/index.js',
  output: {
      filename: './js/bundle.js',
      path: path.resolve(__dirname, 'app')
  },
  plugins: [
  new HTMLWebpackPlugin({
    template: path.resolve(__dirname, `./index.html`),
    filename: 'index.html',
    minify: {
      collapseWhitespace: isProd
    }
  }),
  new CleanWebpackPlugin(),
  new MiniCssExtractPlugin({
    filename: `./css/${filename('css')}`
  })
  ],
  module: {
    rules:[
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.s[ac]ss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      }
    ]
  }
}