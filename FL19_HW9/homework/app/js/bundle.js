(() => {
 'use strict';var e={d:(t,r) => {
 for(let n in r){
 e.o(r,n)&&!e.o(t,n)&&Object.defineProperty(t,n,{enumerable:!0,get:r[n]}) 
} 
},o:(e,t) => Object.prototype.hasOwnProperty.call(e,t)};e.d({},{h:() => r});const t=() => {
 const e=document.querySelector('.days');
 document.querySelector('.date-month').innerHTML=['January','February','March','April','May','June','July','August',
 'September','October','November',
 'December'][r.getMonth()],document.querySelector('.date-year').innerHTML=r.getFullYear();
 const t=new Date(r.getFullYear(),r.getMonth()+1,0).getDate(),
 n=new Date(r.getFullYear(),r.getMonth(),0).getDate(),o=r.getDay(),
 a=7-new Date(r.getFullYear(),r.getMonth()+1,0).getDay()-1;let c='';
 for(let e=o;e>0;e--){
 c+=`<div class="prev-date">${n-e+1}</div>`; 
}for(let e=1;e<=t;e++){
 e===new Date().getDate()&&r.getMonth()===new Date().getMonth()?c+=`<div
  class="today">${e}</div>`:c+=`<div>${e}</div>`; 
}for(let t=1;t<=a;t++){
 c+=`<div class="next-date">${t}</div>`,e.innerHTML=c 
} 
},r=new Date();t(),document.querySelector('.prev').addEventListener('click',() => {
 r.setMonth(r.getMonth()-1),t() 
}),document.querySelector('.next').addEventListener('click',() => {
 r.setMonth(r.getMonth()+1),t() 
}) 
})();