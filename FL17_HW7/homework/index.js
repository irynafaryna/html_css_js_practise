function getAge(date){
  let today = new Date();
  let todaysYear = today.getUTCFullYear();
  let todaysDay = today.getDate();
  let userYear = date.getUTCFullYear();
  let userDay = date.getDate();
  if(userDay > todaysDay){
    return todaysYear - userYear - 1;
  } else{
    return todaysYear - userYear;
  }
}

function getWeekDay(date){
  let day = new Date(date);
  let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return days[day.getDay()] ;
}

function getAmountDaysToNewYear(){
  let chrs = new Date('01/01/2022');  
  let today = new Date(Date.now());  
  let time_difference = chrs.getTime() - today.getTime(); 
  let days_difference = time_difference/(1000 * 60 * 60 * 24); 
  return Math.round(days_difference);
}

function getProgrammersDay(year) {
  let date;
  if (year % 4===0 && year%100 !== 0 ){
    date = new Date(year, 8, 12);
  } else {
    date = new Date(year, 8, 13);
  }
  return date.getDate() + ' Sep ' + date.getUTCFullYear() + ' (' + getWeekDay(date) + ')';
}

function howFarIs(string){
  string = string.toLowerCase();
  let days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  let today = new Date().getDay();
  let currentDay = days.indexOf(string);
  let diff = currentDay - today;
  if (today !== currentDay){
    if(diff < 0){
      let result = diff + 7;
      return 'It`s '+ result + ' day(s) left till ' + string;
    } else {
      return 'It`s '+ diff + ' day(s) left till ' + string;
    }
  } else {
    return 'Hey, today is ' + string + ' =)';
  }
}

function isValidIdentifier(string){
  return /^(^\D)+[\w$]+$/g.test(string);
}

function capitalize(str) {
  return str.replace(/\w\S*/g, function(txt){
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

function isValidAudioFile(string){
  let regex = /^([a-z]+[^_;:+-='"/?%()><|&*^%$#@!~`,])+(?=\.(flac|mp3|alac|aac)+$)\./gi;
  return regex.test(string);
}

function getHexadecimalColors(string){
  let regex = /#([a-f0-9]{3}){1,2}\b/gi;
  if (regex.test(string)){
  return string.match(regex);
  } else {
    return [];
  }
}

function isValidPassword(string){
  let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
  return regex.test(string);
}

function addThousandsSeparators(string) {
  return string.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function getAllUrlsFromText(string){
  let regex = /(https?:\/\/[^\s]+)/g;
  if (regex.test(string)){
    return string.match(regex);
    } else {
      return [];
  }
}
