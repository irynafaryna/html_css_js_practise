class Toy {
  constructor(name, price) {
    this.name = name;
    this.price = String(price);
  }

  getToyInfo() {
    return `The toy name is "${this.name}". It costs ${this.price} dollars.`;
  }
}

class Teddy extends Toy {
  constructor (name, price) {
    super(name, price);
    this.material = 'cotton';
  }

  getMaterialInfo() {
    return `The toy "${this.name}" was made of ${this.material}.`;
  }
}

class Wooden extends Toy {
  constructor (name, price) {
    if (Wooden.exists) {
      return Wooden.instance;
    }
    super(name, price);
    Wooden.instance = this;
    Wooden.exists = true;
    this.material = 'woden';
  }

  getMaterialInfo() {
    return `The toy "${this.name}" was made of ${this.material}.`;
  }

}

class Plastic extends Toy {
  constructor (name, price) {
    super(name, price);
    this.material = 'plastic';
  }

  getMaterialInfo() {
    return `The toy "${this.name}" was made of ${this.material}.`;
  }
}

class Factory {
  constructor() {
    this.toys = [];
  }

  getToy(name) {
    return this.toys.find(toy => toy.name === name)
  }

  produce(name, price, type) {
    let candidate = this.getToy(name);
    if (candidate) {
      return candidate;
    }
    let newToy;
    switch(type) {
      case 'teddy':
        newToy = new Teddy(name, price);
        break;
      case 'wooden':
        newToy = new Wooden(name, price); 
        break;
      case 'plastic':
        newToy = new Plastic(name, price); 
        break;
      default:
        newToy = new Plastic(name, price); 
    }
    this.toys.push(newToy)
    return newToy; 
  }
}

class Car {
  constructor (name, host) {
    this.name = name;
    this.host = host;
  }

  carSound() {
    return 'Usual car sound';
  }
}

function ambulanceCar(car) {
  car.ambulanceSound = function() {
    return 'Siren sound';
  }
  return car;
}
