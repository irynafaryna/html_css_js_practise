/* START TASK 1: Your code goes here */
let table = document.getElementById('table');
let cells = table.getElementsByTagName('td');

table.addEventListener('click', changeBackground);

function changeBackground(el){
  if(!el.target.classList.contains('first-td') && !el.target.classList.contains('special-td') 
  && !el.target.classList.contains('yellow')){
  el.target.style.background = 'yellow';
  } else if (el.target.classList.contains('first-td')) {
    let row = el.target.closest('tr');
    let cell = row.querySelectorAll('td');
    cell.forEach(element => {
      element.style.background = 'blue';
    });
  } else if(el.target.classList.contains('special-td')){
    for (let i = 0; i < cells.length; i++){
      cells[i].style.background = 'green';
    }
  }
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
let button = document.getElementById('button');

button.onclick = function(){

  let inputValue = document.getElementById('number').value;
  let resultblock = document.getElementById('resultblock');
  let regex = /^\+380\d{3}\d{2}\d{2}\d{2}$/;

  if(regex.test(inputValue)){
    resultblock.innerHTML = 'Data was successfully sent.'
    resultblock.style.background='green'
  } else{
    resultblock.innerHTML = 'Type number does not follow format - +380*********'
    resultblock.style.background='pink'
  }
}

/* END TASK 2 */

/* START TASK 3: Your code goes here */
let ball = document.getElementById('ball') 
let court = document.getElementById('task3') 
let left = document.getElementById('left') 
let right = document.getElementById('right') 
let h2 = document.createElement('h2') 
let score = document.createElement('div') 
let teamA = document.createElement('p') 
let teamB = document.createElement('p') 
let scoreA =0 
let scoreB =0 
 
 
score.className= 'score' 
teamA.innerHTML=`Team A:${scoreA}` 
teamB.innerHTML=`Team B:${scoreB}` 
court.after(score) 
score.appendChild(teamA) 
score.appendChild(h2) 
score.appendChild(teamB) 
 
court.onclick = function(event){ 
    let courtCoords = this.getBoundingClientRect(); 
    let ballCoords = { 
       top: event.clientY - courtCoords.top - court.clientTop - ball.clientHeight / 2, 
       left: event.clientX - courtCoords.left - court.clientLeft - ball.clientWidth / 2 
     }; 
       if (ballCoords.top<0){ 
            ballCoords.top = 0 
        } 
       if (ballCoords.left<0) { 
       ballCoords.left = 0 
       } 
 
       ball.style.left=ballCoords.left+'px' 
       ball.style.top=ballCoords.top+'px' 
} 
 
left.onclick= function(){ 
    scoreB++ 
    teamB.innerHTML=`Team B:${scoreB}` 
    h2.style.color = 'red' 
    h2.innerHTML = 'Team B Score!' 
    setTimeout(() => { 
        h2.innerHTML='' 
        ball.style.left=285 + 'px' 
    }, 3000) 
} 
right.onclick= function(){ 
    scoreA++ 
    teamA.innerHTML=`Team A:${scoreA}` 
    h2.style.color = 'blue' 
    h2.innerHTML = 'Team A Score!' 
    setTimeout(() => { 
        h2.innerHTML='' 
        ball.style.left=285 + 'px' 
    }, 3000) 
}
/* END TASK 3 */
